import 'dart:math';

import 'package:flutter/material.dart';

import 'UI/screens.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(

        body: SafeArea(
          child:Center(
            child: An(),
          ),
        ),
      ),
    );
  }
}


class An extends StatefulWidget {
  @override
  _AnState createState() => _AnState();
}

class _AnState extends State<An> {


  @override
  Widget build(BuildContext context) {
    Random r = Random();
    return GestureDetector(
      child: AnimatedContainer(
          duration: Duration(milliseconds: 500),
          width: 200,
          height: 200,
          color: Color.fromRGBO(r.nextInt(255), r.nextInt(255), r.nextInt(255), 1),
      ),
      onTap: ()=>setState(() {}),
    );
  }
}
