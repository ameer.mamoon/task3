import 'package:flutter/material.dart';

class InputWidget extends StatelessWidget {

  static DateTime? _dateTime ;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      width: double.infinity,
      height: double.infinity,
      child: Column(
        children: [
          Row(
            children: [
              Expanded(child: TextField()),
              IconButton(
                icon: Icon(Icons.date_range),
                onPressed: () async {
                  var now = DateTime.now();

                  _dateTime = await showDatePicker(
                      context: context,
                      initialDate: now,
                      firstDate: DateTime(now.year),
                      lastDate: DateTime(now.year+2)
                  );
                  print(_dateTime);
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
