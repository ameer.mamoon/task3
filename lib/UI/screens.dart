import 'package:flutter/material.dart';
import 'package:tasks/STM/controller.dart';
import 'package:tasks/STM/model.dart';
import 'package:tasks/STM/view.dart';
import 'package:tasks/UI/elements.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: TasksList(),
      floatingActionButton: Builder(
        builder: (context) {
          return FloatingActionButton(
            onPressed: (){
              // TaskController.controller.add(TaskModel.map({
              // }));
              showModalBottomSheet(context: context, builder:(c)=> InputWidget());

            },
            child: Icon(Icons.add),
          );
        }
      ) ,
    );
  }
}


