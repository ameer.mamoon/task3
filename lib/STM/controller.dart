import 'package:get/get.dart';
import 'package:tasks/DB/DBhelper.dart';
import 'package:tasks/STM/model.dart';


class TaskController extends GetxController{
  List<TaskModel> _models = [];


  List<TaskModel> get models => _models;

  static final TaskController controller = Get.put(TaskController._());

  TaskController._();


  @override
  void onInit() async {
    _models = await DBHelper().getTasks();
    update();
  }

  Future<void> add(final TaskModel model)async{

    await DBHelper().insertTask(model);
    _models = await DBHelper().getTasks();

    update();
  }

  Future<void> remove(final TaskModel model) async{

    await DBHelper().deleteTask(model.id);
    _models = await DBHelper().getTasks();

    update();
  }

}