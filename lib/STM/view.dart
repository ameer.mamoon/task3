import 'package:flutter/material.dart';
import 'package:tasks/STM/model.dart';
import 'package:get/get.dart';

import 'controller.dart';

class TasksList extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return GetBuilder<TaskController>(
      init: TaskController.controller,
      builder: (controller) {
        return ListView.builder(
            itemCount: controller.models.length,
            itemBuilder: (context,i){
              return TaskWidget(controller.models[i]);
            }
        );
      }
    );
  }
}

class TaskWidget extends StatelessWidget {
  final TaskModel model;


  TaskWidget(this.model);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: Text(model.id.toString()),
        subtitle: Text(model.dateTime.toString()),
        trailing: IconButton(
          icon: Icon(Icons.delete),
          onPressed: (){
            TaskController.controller.remove(model);
          },
        ),
      ),
    );
  }
}
