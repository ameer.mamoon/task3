
class TaskModel {

  final int id ;
  final DateTime dateTime ;
  final String title ;

  TaskModel({
    required this.id,
    required this.dateTime,
    required this.title
  }
      );

  factory TaskModel.map(Map<String,dynamic> m) =>
      TaskModel(
        id: m['id'] ?? -1,
        title: m['title'] ?? 'null',
        dateTime: (m['dateTime'] != null) ? DateTime.parse(m['dateTime']) :
            DateTime.now(),
      );

  Map<String,dynamic> toMap() => {
    'title':title,
    'dateTime': dateTime.toString()
  };


  @override
  String toString() {

    return 'TaskModel{id: $id, dateTime: $dateTime, title: $title}';
  }
}